package model.data_structures;

import junit.framework.TestCase;

public class SimpleLinkedListTest<T> extends TestCase
{
	private SimpleLinkedList<Integer> listaEnlazada;
	public void setupEscenario1()
	{
		listaEnlazada=new SimpleLinkedList<Integer>();
	}

	public void setupEscenario2()
	{
		setupEscenario1();
		listaEnlazada.add(1);
		listaEnlazada.add(2);
		listaEnlazada.add(3);
		listaEnlazada.add(4);
		listaEnlazada.add(5);
		listaEnlazada.add(6);
		listaEnlazada.add(7);
		listaEnlazada.add(8);
		listaEnlazada.add(9);
		listaEnlazada.add(10);
	}
	public void testDoubleLinkedList()
	{
		setupEscenario1();
		assertTrue( "No se inicializo correctamente el iterador", listaEnlazada.iterator()!=null);
	}
	/**
	 * Este metodo permite probar tanto el metodo getSize() como el metodo add()
	 */
	public void testGetSize() {
		setupEscenario2();
		int cantidad= listaEnlazada.getSize();
		assertEquals("No se a�adieron todos los elementos",10,cantidad);
	}
	public void testGetElement()
	{
		setupEscenario2();
		int numero= listaEnlazada.getElement(7); 
		assertEquals("No se consiguio el elemento correcto",3,numero);
		numero=listaEnlazada.getElement(9);
		assertEquals("No se consiguio el elemento correcto",1,numero);

	}
	public void testAddAtK()
	{
		setupEscenario2();

		listaEnlazada.addAtK(2, 12);
		int numero=listaEnlazada.getElement(2);
		assertEquals("No se inserto donde se esperaba",12,numero);




	}
	public void testGetCurrentElement()
	{
		setupEscenario2();
		int nodo =listaEnlazada.getCurrentElement();
		assertEquals("El elemento no es el buscado",10, nodo);	
	}
	public void testDelete()
	{
		setupEscenario2();
		listaEnlazada.delete(4);
		int cant= listaEnlazada.getSize();
		assertEquals("No se elimino nada", 9, cant);
	}
	public void testDeleteAtK()
	{
		setupEscenario2();
		listaEnlazada.deleteAtK(9);
		int cant= listaEnlazada.getSize();
		assertEquals("No se elimino nada", 9, cant);
	}
	public void testNext()
	{
		setupEscenario2();
		boolean hay=listaEnlazada.next();
		assertTrue("Error al devolverse en la lista", hay);

	}

}

