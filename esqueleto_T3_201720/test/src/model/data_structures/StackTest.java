package model.data_structures;

import junit.framework.TestCase;

public class StackTest extends TestCase
{
	private Stack<Integer> pila;
	public void setupEscenario1()
	{
		pila=new Stack<Integer>();
	}
	public void setupEscenario2()
	{
		setupEscenario1();
		pila.push(1);
		pila.push(2);
		pila.push(3);
		pila.push(4);
		pila.push(5);
		pila.push(6);
		pila.push(7);
		
	}

	public void testSize()
	{
		setupEscenario2();
		int cantidad=pila.size();
		assertEquals("no se contaron todos los elementos",7,cantidad);
	}
	public void testPop()
	{
		setupEscenario2();
		int elemento=pila.pop();
		int cantidad=pila.size();
		assertEquals("no se elimino ningun elemento",6,cantidad);
		assertEquals("no se elimino el elemento correcto",7,elemento);
	}
	
}
