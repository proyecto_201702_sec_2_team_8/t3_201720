package model.data_structures;

import junit.framework.TestCase;

public class QueueTest extends TestCase
{
	private Queue<String> fila;
	public void setupEscenario1()
	{
		fila=new Queue<String>();
	}
	public void setupEscenario2()
	{
		setupEscenario1();
		fila.enqueue("uno");
		fila.enqueue("dos");
		fila.enqueue("tres");
		fila.enqueue("cuatro");
		fila.enqueue("cinco");
		fila.enqueue("seis");
		fila.enqueue("siete");
	}

	public void testSize()
	{
		setupEscenario2();
		int cantidad=fila.size();
		assertEquals("no se contaron todos los elementos",7,cantidad);
	}
	public void testDequeue()
	{
		setupEscenario2();
		String elemento=fila.dequeue();
		int cantidad=fila.size();
		assertEquals("no se elimino ningun elemento",6,cantidad);
		assertEquals("no se elimino el elemento correcto","uno",elemento);
	}


}
