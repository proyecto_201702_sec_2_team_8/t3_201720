package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;






import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import model.data_structures.IStack;
import model.data_structures.Queue;
import model.data_structures.SimpleLinkedList;
import model.data_structures.Stack;
import model.vo.BusUpdateVO;
import model.vo.StopVO;
import api.ISTSManager;

public class STSManager implements ISTSManager{
	private Queue<BusUpdateVO[]> coladDeActua;
	private SimpleLinkedList<StopVO> listaParadas;
	public STSManager() {
		this.coladDeActua = new Queue<BusUpdateVO[]>();
	}

	@Override
	public void readBusUpdate(File rtFile) {
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(rtFile));
			Gson gson = new GsonBuilder().create();
			BusUpdateVO[] paradas = gson.fromJson(reader, BusUpdateVO[].class);
			coladDeActua.enqueue(paradas);
			reader.close();

		}catch(Exception e)
		{
			e.printStackTrace();	
		}
	}
	public IStack<StopVO> listStops(Integer tripID) 
	{
		Stack<StopVO> pilaParadas=new Stack<StopVO>();
		Queue<BusUpdateVO[]> colaNueva=coladDeActua;
		while(colaNueva.size()>0)
		{
			BusUpdateVO[] nuevo= colaNueva.dequeue();
			for(int i=0;i<nuevo.length;i++)
			{
				BusUpdateVO singular=nuevo[i];
				if(singular.darId()==tripID)
				{
					double lat1= singular.darLatitud();
					double lon1=singular.darLongitud();
					for(int j=0;j<listaParadas.getSize();j++)
					{
						StopVO parada=listaParadas.getElement(j);
						double lat2= parada.darLatitud();
						double lon2=parada.darLongitud();
						double distancia =getDistance(lat1, lon1, lat2, lon2);
						if(distancia<=70)
						{
							pilaParadas.push(parada);
						}
					}
				}
			}
		}
		System.out.println(pilaParadas.size());
		return pilaParadas;
	}


	@Override
	public void loadStops() 
	{
		listaParadas=new SimpleLinkedList<StopVO>();
		try
		{
			File archivo= new File("data2/stops.txt");
			BufferedReader bf=new BufferedReader(new FileReader(archivo));
			bf.readLine();
			String linea = bf.readLine();
			while(linea!=null)
			{
				String[] partes=linea.split(",");
				StopVO nuevo=new StopVO(Integer.valueOf((partes[0].trim())),partes[2].trim(), partes[3].trim(), Double.valueOf(partes[4].trim()),Double.valueOf((partes[5]).trim()), partes[6].trim(), partes[7].trim()	, partes[8].trim());
				listaParadas.addAtEnd(nuevo);
				linea = bf.readLine();
			}	

		}
		catch(Exception e)

		{
			e.printStackTrace();
		}


	}
	public double getDistance(double lat1, double lon1, double lat2, double lon2) {
		// TODO Auto-generated method stub
		final int R = 6371*1000; // Radious of the earth

		Double latDistance = toRad(lat2-lat1);
		Double lonDistance = toRad(lon2-lon1);
		Double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + 
				Math.cos(toRad(lat1)) * Math.cos(toRad(lat2)) * 
				Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
		Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		Double distance = R * c;

		return distance;

	}

	private Double toRad(Double value) 
	{
		return value * Math.PI / 180;
	}


}



