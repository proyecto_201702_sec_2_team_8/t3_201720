package model.vo;

public class StopVO{



	/**
	 * Representation of a Stop object
	 */

	private Integer stop_id;
	private Integer stop_code;
	private String stop_name;
	private String stop_desc;
	private double stop_lat;
	private double stop_lon;
	private String zone_id;
	private String stop_url;
	private String location_type;
	private String parent_station;


	public StopVO(Integer stop_id, String stop_name, String stop_desc, double stop_lat, double stop_lon, String zone_id,	String stop_url, String location_type )
	{
		this.stop_id = stop_id;
		this.stop_code = stop_code;
		this.stop_name = stop_name;
		this.stop_desc = stop_desc;
		this.stop_lat = stop_lat;
		this.stop_lon = stop_lon;
		this.zone_id = zone_id;
		this.stop_url = stop_url;
		this.location_type = location_type;
		this.parent_station = parent_station;

	}
	/**
	 * @return id - stop's id
	 */
	public int id() 
	{
		// TODO Auto-generated method stub
		return stop_id;
	}

	public String name()
	{
		return stop_name;
	}

	/**
	 * @return name - stop name
	 */
	public String getZone() 
	{
		// TODO Auto-generated method stub
		return zone_id;
	}
	public double darLatitud()
	{
		return stop_lat;
	}
	
	public double darLongitud()
	{
		return stop_lon;
	}


}



