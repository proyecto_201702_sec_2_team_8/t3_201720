package model.data_structures;

public class Queue<T> implements IQueue<T>
{
	private SimpleLinkedList<T> lista;
	public Queue()
	{
		lista=new SimpleLinkedList<>();
	}
	public void enqueue(T paradas)
	{
		lista.add(paradas);
	}
	public T dequeue()
	{
		Integer tamanio=lista.getSize();
		T ultimoElemento=lista.getElement(tamanio-1);
		lista.deleteAtK(tamanio-1);
		return ultimoElemento;
	}
	public Integer size()
	{
		return lista.getSize();
	}
}
