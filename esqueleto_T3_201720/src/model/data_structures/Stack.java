package model.data_structures;

public class Stack<T> implements IStack<T>
{

	private SimpleLinkedList<T> lista;
	public Stack()
	{
		lista=new SimpleLinkedList<>();
	}

	public void push(T item)
	{
		lista.add(item);	
	}
	public T pop()
	{
		T elemento=lista.getElement(0);
		lista.deleteAtK(0);
		return elemento;
	}
	public Integer size()
	{
		return lista.getSize();
	}
}
